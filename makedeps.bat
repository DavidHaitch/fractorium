REM Make final output folder
mkdir Deps

REM uncomment if cl message is "Cannot open include file: 'stddef.h'"
REM C:\Program Files (x86)\Microsoft Visual Studio\2019\Community\VC\Auxiliary\Build\vcvars64.bat

REM Move to parent of deps folders
cd ..
git clone https://github.com/madler/zlib.git
git clone -b 4.5.5 https://github.com/opencv/opencv.git
git clone https://github.com/glennrp/libpng.git
git clone https://github.com/GNOME/libxml2.git
git clone https://github.com/g-truc/glm.git
git clone -b v3.1.3 https://github.com/AcademySoftwareFoundation/openexr.git

REM libjpeg
copy fractorium\Builds\MSVC\WIN32.MAK libjpeg
cd libjpeg
nmake /f makefile.vc setup-v16 CPU=i386
nmake nodebug=1 /f makefile.vc libjpeg.lib CPU=i386
copy libjpeg.lib ..\fractorium\Deps
cd ..

REM zlib
cd zlib
nmake -f win32/Makefile.msc all
copy zlib.lib ..\fractorium\Deps
cd ..

@REM REM libxml2
@REM cd libxml2\win32
@REM cscript configure.js compiler=msvc iconv=no zlib=yes include=..\..\zlib lib=..\..\fractorium\Deps
@REM nmake /f Makefile.msvc all
@REM cd bin.msvc
@REM copy libxml2.dll ..\..\..\fractorium\Deps
@REM copy libxml2.lib ..\..\..\fractorium\Deps
@REM cd ..\..\..

@REM REM libpng
@REM cd libpng
@REM mkdir zlib
@REM copy ..\zlib\zlib.lib zlib
@REM copy ..\zlib\zlib.h zlib
@REM copy ..\zlib\zconf.h zlib
@REM nmake -f scripts\makefile.vcwin32 all
@REM copy libpng.lib ..\fractorium\Deps
@REM cd ..

@REM REM openexr
@REM cd openexr
@REM SET current=%cd%

@REM if not exist ".\output" mkdir .\output

@REM REM cd ..\OpenEXR

@REM cmake -G "Visual Studio 16 2019"^
@REM       -A x64^
@REM       -DCMAKE_PREFIX_PATH="%current%\output"^
@REM       -DCMAKE_INSTALL_PREFIX="%current%\output"^
@REM       -DILMBASE_PACKAGE_PREFIX="%current%\output" ^
@REM       -DZLIB_ROOT="..\zlib"^
@REM 	  -DOPENEXR_BUILD_SHARED_LIBS="ON"^
@REM 	  -DOPENEXR_BUILD_VIEWERS="OFF"^
@REM 	  -DOPENEXR_BUILD_STATIC_LIBS="OFF"^
@REM 	  -DOPENEXR_BUILD_PYTHON_LIBS="OFF"^
@REM 	  -DOPENEXR_ENABLE_TESTS="OFF"^
@REM       .\

@REM cmake --build . --target install --config Release

@REM xcopy %current%\output\Include %current%\..\fractorium\Deps\Include\ /S /Y
@REM xcopy %current%\output\bin\Iex-3_1.dll %current%\..\fractorium\Deps\ /Y
@REM xcopy %current%\output\bin\IlmThread-3_1.dll %current%\..\fractorium\Deps\ /Y
@REM xcopy %current%\output\bin\Imath-3_1.dll %current%\..\fractorium\Deps\ /Y
@REM xcopy %current%\output\bin\OpenEXR-3_1.dll %current%\..\fractorium\Deps\ /Y
@REM xcopy %current%\output\lib\*.lib %current%\..\fractorium\Deps\ /Y

REM opencv
cd opencv
SET current=%cd%
if not exist "..\opencv_build" mkdir ..\opencv_build

REM cd ..\opencv

cmake -G"Visual Studio 16 2019"^
      -A x64^
      -B..\opencv_build^
      -DBUILD_PERF_TESTS:BOOL=OFF^
      -DBUILD_TESTS:BOOL=OFF^
      -DBUILD_DOCS:BOOL=OFF^
      -DWITH_CUDA:BOOL=OFF^
      -DBUILD_EXAMPLES:BOOL=OFF^
      -DINSTALL_CREATE_DISTRIB=ON^
      -DCMAKE_INSTALL_PREFIX="%current%\output"^
      -S.\

cmake --build ..\opencv_build --target install --config Debug
cmake --build ..\opencv_build --target install --config Release

cd %current%

xcopy %current%\output\include %current%\..\fractorium\Deps\Include\ /S /Y
xcopy %current%\output\x64\vc16\lib\*.lib %current%\..\fractorium\Deps\ /Y
xcopy %current%\output\x64\vc16\bin\opencv_world455.dll %current%\..\fractorium\Deps\ /Y
xcopy %current%\output\x64\vc16\bin\opencv_world455d.dll %current%\..\fractorium\Deps\ /Y

cd ..\fractorium
